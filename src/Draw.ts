import { GetData } from "./Data";
import { IsTargetGroupInOutfit } from "./Outift/OutfitCtrl";
import { CurTaskTime } from "./Task/Task";

export function ADCSDrawCharacter(C: Character, X: number, Y: number, Zoom: number) {
    if (Player && Player.MemberNumber && C.MemberNumber === Player.MemberNumber
        && CurrentScreen === "ChatRoom" && ChatRoomSpace !== 'Asylum') {
        if (!IsTargetGroupInOutfit(Player, 'ItemNeck')) return;

        let rules = GetData().Effects;
        let punish = GetData().ADCS.Punish;
        let score = GetData().ADCS.Score;
        let time = CurTaskTime();

        DrawEmptyRect(X + 50 * Zoom, Y + 860 * Zoom, 100 * Zoom, 40 * Zoom, "Black");
        DrawRect(X + 52 * Zoom, Y + 862 * Zoom, 96 * Zoom, 36 * Zoom, "#D0FF94");
        DrawText(score.toString(), X + 100 * Zoom, Y + 881 * Zoom, "Black", "White");

        DrawEmptyRect(X + 50 * Zoom, Y + 900 * Zoom, 100 * Zoom, 40 * Zoom, "Black");
        DrawRect(X + 52 * Zoom, Y + 902 * Zoom, 96 * Zoom, 36 * Zoom, "#FFD094");
        DrawText(punish.toString(), X + 100 * Zoom, Y + 921 * Zoom, "Black", "White");

        if (time) {
            let remainT = time + GetData().ADCS.ResponseTime - Date.now();
            DrawEmptyRect(X + 350 * Zoom, Y + 860 * Zoom, 100 * Zoom, 40 * Zoom, 'White', 2);
            DrawRect(X + 352 * Zoom, Y + 862 * Zoom, 96 * Zoom, 36 * Zoom, 'Black');
            DrawText((remainT / 1000).toFixed(1), X + 399 * Zoom, Y + 882 * Zoom, 'White', "Silver");
        }
    }
}