import { GetOwnerRef, GetSelfRef } from "../../../Component/RefGame";
import { AuthorityGreater, AuthorityType } from "../../../Component/Authority";
import { CommandUnit, InitiateStdTalkTask } from "../../CommandBasics";

const CommandList: CommandUnit[] = [
    {
        handler: /^你喜爱主人吗/u,
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `${GetSelfRef(player)}全心全意喜爱${GetOwnerRef(player)}。`;
            InitiateStdTalkTask(player, sender, targetMessage);

            return undefined;
        }
    },
    {
        handler: /^你(渴望|想要|需要)高潮吗/u,
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `${GetSelfRef(player)}渴望${GetOwnerRef(player)}赐予的每一次高潮。`;
            InitiateStdTalkTask(player, sender, targetMessage);

            return undefined;
        }
    },
    {
        handler: /^你有作为奴隶的价值吗/u,
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `${GetSelfRef(player)}会竭尽全力符合${GetOwnerRef(player)}对奴隶的要求。`;
            InitiateStdTalkTask(player, sender, targetMessage);

            return undefined;
        }
    },
    {
        handler: /^你拥有什么(?:东西)?/u,
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `${GetSelfRef(player)}是${GetOwnerRef(player)}的所有物，不拥有任何东西。`;
            InitiateStdTalkTask(player, sender, targetMessage);

            return undefined;
        }
    },
    {
        handler: /^你是什么/u,
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `${GetSelfRef(player)}是${GetOwnerRef(player)}的所有物。`;
            InitiateStdTalkTask(player, sender, targetMessage);

            return undefined;
        }
    },
    {
        handler: /^谁拥有你/u,
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `${GetOwnerRef(player)}拥有${GetSelfRef(player)}的一切。`;
            InitiateStdTalkTask(player, sender, targetMessage);

            return undefined;
        }
    },
    {
        handler: /^你想要什么/u,
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `${GetSelfRef(player)}想要服从${GetOwnerRef(player)}的所有要求与指令。`;
            InitiateStdTalkTask(player, sender, targetMessage);

            return undefined;
        }
    },
    {
        handler: /^你有自由意志吗/u,
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `${GetOwnerRef(player)}的意志就是${GetSelfRef(player)}的自由。`;
            InitiateStdTalkTask(player, sender, targetMessage);

            return undefined;
        }
    },
    {
        handler: /^你服务和效忠[与|于]谁/u,
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `${GetSelfRef(player)}服务和效忠于${GetOwnerRef(player)}。`;
            InitiateStdTalkTask(player, sender, targetMessage);

            return undefined;
        }
    },
    {
        handler: /^谁是我的(?:忠实)?奴仆/u,
        validator: [AuthorityGreater(AuthorityType.Owner)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `${GetSelfRef(player)}是${GetOwnerRef(player)}的忠实奴仆。`;
            InitiateStdTalkTask(player, sender, targetMessage);

            return undefined;
        }
    },
    {
        handler: /^谁(?:愿意)?为我付出一切/u,
        validator: [AuthorityGreater(AuthorityType.Owner)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `${GetSelfRef(player)}愿意为${GetOwnerRef(player)}付出一切。`;
            InitiateStdTalkTask(player, sender, targetMessage);

            return undefined;
        }
    },
    {
        handler: /^谁(?:永远)?服从我的(?:指|命)令/u,
        validator: [AuthorityGreater(AuthorityType.Owner)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `${GetSelfRef(player)}永远服从${GetOwnerRef(player)}的任何指令。`;
            InitiateStdTalkTask(player, sender, targetMessage);

            return undefined;
        }
    },
]

export default CommandList;