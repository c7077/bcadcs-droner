import { DroneName } from "../../../Contents";
import { DataEffect, GetData, SaveData } from "../../../Data";
import { IncreasePunish, IncreaseScore } from "../../../Component/ScoreControl";
import { queueShockAction } from "../../../Component/ShockProvider";
import { AuthorityGreater, AuthorityType } from "../../../Component/Authority";
import { CommandUnit } from "../../CommandBasics";
import { IsInCollar } from "../../../Outift/OutfitCtrl";
import { ChatRoomStdSendAction } from "../../../Locale";

const CommandList: CommandUnit[] = [
    {
        handler: /^good slave/iu,
        validator: [AuthorityGreater(AuthorityType.Owner)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            ChatRoomStdSendAction(`${IncreaseScore()}。`);
            SaveData();

            return undefined;
        }
    },
    {
        handler: /^penalty/iu,
        validator: [AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            ChatRoomStdSendAction(`${IncreasePunish()}。`);
            SaveData();

            return undefined;
        }
    },
    {
        handler: /^award credit/iu,
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            if (GetData().ADCS.Punish > 0) {
                ChatRoomStdSendAction(`Penalty exists, ${DroneName(player)} cannot be awarded.`);
                return;
            }

            let v = Math.floor(GetData().ADCS.Score / 2);
            let bonus = Math.max(v * 5, v * 10 - 150, v * 15 - 600);

            if (player.Money) {
                if (typeof player.Money !== 'number')
                    player.Money = 0;

                GetData().ADCS.Score -= v;
                CharacterChangeMoney(player, bonus);
                ReputationProgress('Dominant', -v);
                SaveData();
                ServerPlayerSync();
                ServerPlayerReputationSync();
            }
            ChatRoomStdSendAction(`${v} Bonus is converted to ${bonus} credit for ${DroneName(player)}, who becomes more submissive.`);
            return undefined;
        }
    },
    {
        handler: /^shock punishment/iu,
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            queueShockAction(GetData().ADCS.Punish);
            SaveData();
            ChatRoomStdSendAction(`${IncreasePunish()}。`);
            return undefined;
        }
    },
    {
        handler: /^increase skill[\\p{P}\\s~]*(evasion|bondage|Self-bondage|willpower|lock-picking)/iu,
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            if (GetData().ADCS.Punish > 0) {
                ChatRoomStdSendAction(`Penalty exists, ${DroneName(player)} cannot be awarded.`);
                return;
            }

            let v = Math.floor(GetData().ADCS.Score / 2);
            let skill = result[1].toLowerCase();

            const SkillMap = new Map<string, SkillType>([
                ['evasion', 'Evasion'],
                ['bondage', 'Bondage'],
                ['self-bondage', 'SelfBondage'],
                ['willpower', 'Willpower'],
                ['lock-picking', 'LockPicking'],
            ])

            let skType = SkillMap.get(skill) as SkillType;
            GetData().ADCS.Score -= v;
            SkillProgress(player, skType, v);
            SaveData();
            ServerPlayerSkillSync();

            ChatRoomStdSendAction(`System intervenes the mind of ${DroneName(player)}, ${v} Bonus is used in increasing the exp of ${skill}.`);

            return undefined;
        }
    },
    {
        handler: /^(cancel )?(?:award full gear)/iu,
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            if (result[1]) {
                GetData().Effects = GetData().Effects.filter(_ => _ !== 'AwardFullGear');
            } else if (!GetData().Effects.includes('AwardFullGear')) {
                GetData().Effects.push('AwardFullGear');
            }
            SaveData();
            ChatRoomStdSendAction(`Now ${DroneName(player)} in full gear for 1 hour ${GetData().Effects.includes('AwardFullGear') ? 'will' : 'won\'t'} get 1 bonus.`);

            return undefined;
        }
    },
    {
        handler: /^(cancel |stop )?(?:punish orgasm)/iu,
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            if (result[1]) {
                DataEffect().set('PunishOrgasm', false);
            } else {
                DataEffect().set('PunishOrgasm', true);
            }
            SaveData();
            ChatRoomStdSendAction(`${DroneName(player)} now ${DataEffect().test('PunishOrgasm') ? 'will' : 'won\'t'} be punished from orgasms.`);

            return undefined;
        }
    },
]

export default CommandList;