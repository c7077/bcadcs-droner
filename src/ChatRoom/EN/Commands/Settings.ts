import { AuthorityGreater, AuthorityType } from "../../../Component/Authority";
import { GetOwnerRef, GetSelfRef, SetOwnerRef, SetSelfRef } from "../../../Component/RefGame";
import { DroneName } from "../../../Contents";
import { DataEffect, GetData, SaveData, defaultResponseTime } from "../../../Data";
import { ModVersion } from "../../../Definitions";
import { ChatRoomStdSendAction } from "../../../Locale";
import { InjectFullbodyWithReport, IsInCollar, ItemDescritpion, ReColorItems } from "../../../Outift/OutfitCtrl";
import { InitiateTalkTask } from "../../../Task/Task";
import { IsOwnedBy } from "../../../utils/Criteria";
import { CommandUnit } from "../../CommandBasics";

const CommandList: CommandUnit[] = [
    {
        handler: /^call yourself[\\p{P}\\s~]*(.*)/iu,
        validator: [AuthorityGreater(AuthorityType.Owner)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let old = GetSelfRef(player);
            if (result[1]) {
                SetSelfRef(player, '');
            }
            else {
                if (!result[2]) return;
                let re = result[2].trim();
                if (!re) return;
                SetSelfRef(player, re);
            }
            let targetMessage = `${old} will call self as ${GetSelfRef(player)}.`;
            InitiateTalkTask(targetMessage, IsOwnedBy(player, sender));

            return undefined;
        }
    },
    {
        handler: /^call your owner[\\p{P}\\s~]*(.*)/iu,
        validator: [AuthorityGreater(AuthorityType.Owner)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            if (result[1]) {
                SetOwnerRef(player, '');
            }
            else {
                if (!result[2]) return;
                let re = result[2].trim();
                if (!re) return;
                SetOwnerRef(player, re);
            }
            let targetMessage = `${GetSelfRef(player)} will call owner as ${GetOwnerRef(player)}.`;
            InitiateTalkTask(targetMessage, IsOwnedBy(player, sender));

            return undefined;
        }
    },
    {
        handler: /^turn (on|off) talk guide/iu,
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let opt = result[1].toLowerCase();

            if (opt === 'off') {
                DataEffect().set('NoTalkGuide', true);
            }
            else if (opt === 'on') {
                DataEffect().set('NoTalkGuide', false);
            }

            CharacterRefresh(player);
            SaveData();
            ChatRoomStdSendAction(`The talk guide of ${DroneName(player)} is turned ${DataEffect().test('NoTalkGuide') ? 'off' : 'on'}`);

            return undefined;
        }
    },
    {
        handler: /^set theme color[\\p{P}\\s]+#([0-9A-F]{6}|[0-9A-F]{3})/iu,
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            GetData().Theme.Color = `#${result[1]}`;
            SaveData();
            ReColorItems(player);
            CharacterRefresh(player);
            ChatRoomCharacterUpdate(player);
            ChatRoomStdSendAction(`Theme color is set to: #${result[1]}`);
            return undefined;
        }
    },
    {
        handler: /^inject gears/u,
        validator: [AuthorityGreater(AuthorityType.Owner)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            let v = InventoryGet(player, 'ItemNeck');
            let r = ItemDescritpion(player, 'ItemNeck');

            if (r === undefined) return;

            if (!v || v.Asset.Name !== r.Name) {
                ChatRoomStdSendAction(`Must wear ${r.Description} to continue.`);
                return;
            }

            let report = InjectFullbodyWithReport(player);

            let injected = report.filter(_ => _.State === 'Injected');
            let changMessage = injected.length > 0 ? `${injected.map(_ => _.GroupDescription).join(`, `)} have been injected.` : `No injection operated.`;

            let tobedone = report.filter(_ => _.State === 'ToBeDone');
            let firstTwo = tobedone.slice(0, 2);
            let toDoMessage = tobedone.length > 0 ? `Wearing ${firstTwo.map(_ => _.Description).join(`, `)} on ${firstTwo.map(_ => _.GroupDescription).join(', ')} respectly can continue with further injection.` : `All possible injections have been operated.`;

            CharacterRefresh(player);
            ChatRoomCharacterUpdate(player);

            ChatRoomStdSendAction(`${changMessage} ${toDoMessage}`);

            return undefined;
        }
    },
    {
        handler: /^set reaction time[\\p{P}\\s~]*(\\d+(?:\\.\\d+)?)?/u,
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            if (result[1]) {
                GetData().ADCS.ResponseTime = defaultResponseTime;
            }
            else {
                let v = Number(result[2])
                GetData().ADCS.ResponseTime = Math.floor(v * 1000);
            }
            ChatRoomStdSendAction(`The reaction time of ${DroneName(player)} is set to ${GetData().ADCS.ResponseTime / 1000} second(s).`);

            return undefined;
        }
    },
    {
        handler: /^(cancel )?grounded/iu,
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            if (result[1]) {
                DataEffect().set('Grounded', false);
            } else {
                DataEffect().set('Grounded', true);
                ChatRoomSlowStop = true;
            }
            SaveData();
            ChatRoomStdSendAction(`${DroneName(player)} is ${DataEffect().test('Grounded') ? 'now' : 'no longer'} grounded.`);

            return undefined;
        }
    },
    {
        handler: /^(cancel )?inject GGTS/iu,
        validator: [IsInCollar, AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character, result: RegExpExecArray) => {
            if (result[1]) {
                DataEffect().set('InjectGGTS', false);
            } else {
                DataEffect().set('InjectGGTS', true);
            }
            SaveData();
            ChatRoomStdSendAction(`${DroneName(player)} is ${GetData().Effects.includes('InjectGGTS') ? 'now' : 'no longer'} protected from GGTS lv6 rule.`);

            return undefined;
        }
    },
    {
        handler: /^(?:maintenance|maintenance info|inspect|inspect status)/u,
        validator: [AuthorityGreater(AuthorityType.Moderator)],
        worker: (player: Character, sender: Character) => {
            if (IsInCollar(player)) {
                SaveData();
                let State = [
                    DataEffect().test('PunishOrgasm') ? 'Punish orgasm' : '',
                    DataEffect().test('NoTalkGuide') ? 'No talk guide' : '',
                    DataEffect().test('InjectGGTS') ? 'Inject GGTS' : '',
                    DataEffect().test('AwardFullGear') ? 'Award Full Gear' : '',
                    DataEffect().test('Grounded') ? 'Grounded' : '',
                    DataEffect().test('LoverNotAsAdmin') ? '' : 'Lovers as Moderator',
                ].filter(_ => _);

                const verMsg = `Advanced Drone Control System™ version v${ModVersion}.`;
                const ctrlMsg = `Controled target: ${player.MemberNumber}.`;
                const bpMsg = `Reward: ${GetData().ADCS.Score}, Penalty: ${GetData().ADCS.Punish}.`;
                const stateMsg = `Current rules: ${State.length > 0 ? State.join(', ') : 'No rules in effect'}.`;

                ChatRoomStdSendAction(`${verMsg} ${ctrlMsg} ${bpMsg} ${stateMsg}`);
            }
            else {
                ChatRoomStdSendAction(`[ADCS] ADCS™ Central Processor is not online.`);
            }

            return undefined;
        }
    },
]

export default CommandList;