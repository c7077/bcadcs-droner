import { CheckPose, InitiatePoseTask } from "../../../Task/Task";
import { AuthorityGreater, AuthorityType } from "../../../Component/Authority";
import { BasicBonusCriteria, BasicPosePenaltyCriteria, CommandUnit } from "../../CommandBasics";

const CommandList: CommandUnit[] = [
    {
        handler: /^raise your hands?(?:[\\p{P}\\s]+(.+))?/iu,
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            const target: AssetPoseName[] = ['Yoked', 'OverTheHead'];
            if (CheckPose(player, target)) return;
            InitiatePoseTask(target, BasicBonusCriteria(player, sender), BasicPosePenaltyCriteria(player, sender, target));

            return result[1];
        }
    },
    {
        handler: /^put your arms (behind|behind back|in front)(?:[\\p{P}\\s]+(.+))?/iu,
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            const opt = result[1].toLowerCase();

            if (['behind', 'behind back'].includes(opt)) {
                const target: AssetPoseName[] = ['BackBoxTie', 'BackElbowTouch', 'BackCuffs'];
                if (CheckPose(player, target)) return;
                InitiatePoseTask(target, BasicBonusCriteria(player, sender), BasicPosePenaltyCriteria(player, sender, target));
            }
            else if (['in front'].includes(opt)) {
                const target: AssetPoseName[] = ['BaseUpper'];
                InitiatePoseTask(target, BasicBonusCriteria(player, sender), BasicPosePenaltyCriteria(player, sender, target));
            }

            return result[2];
        }
    },
    {
        handler: /^(stand|stand up|kneel|kneel down)(?:[\\p{P}\\s]+(.+))?/u,
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            const opt = result[1].toLowerCase();

            if (['stand', 'stand up'].includes(opt)) {
                const target: AssetPoseName[] = ['BaseLower', 'LegsClosed'];
                if (CheckPose(player, target)) return;
                InitiatePoseTask(target, BasicBonusCriteria(player, sender), BasicPosePenaltyCriteria(player, sender, target));
            }
            else if (['kneel', 'kneel down'].includes(opt)) {
                const target: AssetPoseName[] = ['KneelingSpread', 'Kneel'];
                if (CheckPose(player, target)) return;
                InitiatePoseTask(target, BasicBonusCriteria(player, sender), BasicPosePenaltyCriteria(player, sender, target));
            }

            return result[2];
        }
    },
    {
        handler: /^(open|spread|close) your legs(?:[\\p{P}\\s]+(.+))?/u,
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            let opt = result[1].toLowerCase();
            if (['open', 'spread'].includes(opt)) {
                const target: AssetPoseName[] = ['Spread', 'KneelingSpread', 'BaseLower'];
                if (CheckPose(player, target)) return;
                InitiatePoseTask(target, BasicBonusCriteria(player, sender), BasicPosePenaltyCriteria(player, sender, target));
            }
            else if (['close'].includes(opt)) {
                const target: AssetPoseName[] = ['LegsClosed', 'Kneel'];
                InitiatePoseTask(target, BasicBonusCriteria(player, sender), BasicPosePenaltyCriteria(player, sender, target));
            }

            return result[2];
        }
    },
]

export default CommandList;