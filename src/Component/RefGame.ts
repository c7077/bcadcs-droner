import { DroneName } from "../Contents";
import { GetData, SaveData } from "../Data";
import { ChatRoomStdLocalAction, GetRefRegExp, GetString } from "../Locale";
import { IsTargetGroupInOutfit } from "../Outift/OutfitCtrl";
import { IsOwnedBy } from "../utils/Criteria";

export function GetSelfRef(player: Character) {
    if (GetData().ADCS.SelfReference) return GetData().ADCS.SelfReference;
    return CharacterNickname(player);
}

export function SetSelfRef(player: Character, name: string) {
    name = name.trim();

    if (GetRefRegExp().test(name) || DroneName(player) === name) {
        GetData().ADCS.SelfReference = "";
    }
    else GetData().ADCS.SelfReference = name;
    SaveData();
}

export function SelfRefGameChat(player: Character, content: string) {
    if (IsTargetGroupInOutfit(player, 'ItemNeck')) {
        let r = GetRefRegExp().exec(content);
        if (r) {
            ChatRoomStdLocalAction(GetString("ref_game_info", [`${r[0]}`, GetSelfRef(player)]));
            return undefined;
        }
    }
    return content;
}

export function GetOwnerRef(player: Character) {
    if (GetData().ADCS.OwnerReference) {
        return GetData().ADCS.OwnerReference;
    }
    if (player.Ownership)
        return `${player.Ownership.Name}`;
    return '';
}

export function SetOwnerRef(player: Character, name: string) {
    name = name.trim();
    GetData().ADCS.OwnerReference = name;
    SaveData();
}

export function GetRef(player: Character, sender: Character, pronoun: { Deprevated: string, Owner: string, Other: string }) {
    if (PreferenceIsPlayerInSensDep()) {
        return pronoun.Deprevated;
    }
    else {
        if (IsOwnedBy(player, sender)) {
            return pronoun.Owner;
        } else {
            return pronoun.Other;
        }
    }
}