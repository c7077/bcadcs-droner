import { ModSDKModAPI } from "bondage-club-mod-sdk";
import { IsTargetGroupInOutfit } from "../Outift/OutfitCtrl";

export function NoRelease(mod: ModSDKModAPI) {
    mod.hookFunction('CharacterRelease', 20, (args, next) => {
        let C: Character = args[0];
        let Weset: Item[] = []
        if (C.MemberNumber && Player && Player.MemberNumber === C.MemberNumber) {
            Weset = C.Appearance.filter(_ => IsTargetGroupInOutfit(C, _.Asset.Group.Name));
        }
        next(args);
        let fixed = false;
        Weset.forEach(_ => {
            let v = InventoryGet(C, _.Asset.Group.Name);
            if (v) return;
            C.Appearance.push(_);
            fixed = true;
        });
        if (fixed) CharacterRefresh(C);
    });

    mod.hookFunction('InventoryAllow', 20, (args, next) => {
        let C: Character = args[0];
        let asset: Asset = args[1];
        if (C.MemberNumber && Player && Player.MemberNumber === C.MemberNumber && IsTargetGroupInOutfit(C, asset.Group.Name)) {
            return false;
        }
        else return next(args);
    });
}