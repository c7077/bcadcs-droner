type HandleFunction = (player: Character, sender: Character, msg: { Original: string, Garbled: string }) => void;

export class ChatLogHandler {
    _LastTime = 0;
    _handles = new Array<HandleFunction>;

    Run(player: Character) {
        if (!Array.isArray(ChatRoomChatLog)) return;
        for (let L = 0; L < ChatRoomChatLog.length; L++) {
            let tLog = ChatRoomChatLog[L];
            if (tLog.Time > this._LastTime) {
                let sender = ChatRoomCharacter.find(_ => _.MemberNumber && _.MemberNumber === tLog.SenderMemberNumber);
                this._LastTime = tLog.Time;
                this._handles.forEach(f => Player && sender && f(Player, sender, { Original: tLog.Original, Garbled: tLog.Garbled }));
            }
        }
    }

    Register(handle: HandleFunction) {
        this._handles.push(handle);
    }
}