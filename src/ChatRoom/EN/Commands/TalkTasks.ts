import { GetOwnerRef, GetSelfRef } from "../../../Component/RefGame";
import { AuthorityGreater, AuthorityType } from "../../../Component/Authority";
import { CommandUnit, InitiateStdTalkTask } from "../../CommandBasics";

const CommandList: CommandUnit[] = [
    {
        handler: /^do you love your (?:master|mistress)/iu,
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `${GetSelfRef(player)} loves ${GetOwnerRef(player)} with sincere.`;
            InitiateStdTalkTask(player, sender, targetMessage);
            return undefined;
        }
    },
    {
        handler: /^do you want (to )?orgasm/iu,
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `${GetSelfRef(player)} urges every orgasm ${GetOwnerRef(player)} bestows`;
            InitiateStdTalkTask(player, sender, targetMessage);
            return undefined;
        }
    },
    {
        handler: /^are you (slave worthy|a worthy slave)/iu,
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `${GetSelfRef(player)} strives to be worthy as a slave owned by ${GetOwnerRef(player)}.`;
            InitiateStdTalkTask(player, sender, targetMessage);
            return undefined;
        }
    },
    {
        handler: /^what do you own/iu,
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `As a slave of ${GetOwnerRef(player)}, ${GetSelfRef(player)} owns nothing.`;
            InitiateStdTalkTask(player, sender, targetMessage);
            return undefined;
        }
    },
    {
        handler: /^(whose property|what) are you/iu,
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `${GetSelfRef(player)} is a private property of ${GetOwnerRef(player)}.`;
            InitiateStdTalkTask(player, sender, targetMessage);
            return undefined;
        }
    },
    {
        handler: /^who owns you/iu,
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `${GetOwnerRef(player)} owns everything of ${GetSelfRef(player)}.`;
            InitiateStdTalkTask(player, sender, targetMessage);
            return undefined;
        }
    },
    {
        handler: /^what do you want/iu,
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `${GetSelfRef(player)} wants to obey every order from ${GetOwnerRef(player)}.`;
            InitiateStdTalkTask(player, sender, targetMessage);
            return undefined;
        }
    },
    {
        handler: /^do you have free will/iu,
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `My will is ${GetOwnerRef(player)}'s command.`;
            InitiateStdTalkTask(player, sender, targetMessage);
            return undefined;
        }
    },
    {
        handler: /^who do you serve and obey/iu,
        validator: [AuthorityGreater(AuthorityType.Allowed)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `${GetSelfRef(player)} serves and obeys ${GetOwnerRef(player)}.`;
            InitiateStdTalkTask(player, sender, targetMessage);
            return undefined;
        }
    },
    {
        handler: /^who is my loyal servant/iu,
        validator: [AuthorityGreater(AuthorityType.Owner)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `${GetSelfRef(player)} is the loyal servant of ${GetOwnerRef(player)}.`;
            InitiateStdTalkTask(player, sender, targetMessage);
            return undefined;
        }
    },
    {
        handler: /^who gives everything for me/iu,
        validator: [AuthorityGreater(AuthorityType.Owner)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `${GetSelfRef(player)} give everything for ${GetOwnerRef(player)}.`;
            InitiateStdTalkTask(player, sender, targetMessage);
            return undefined;
        }
    },
    {
        handler: /^who obeys my orders/iu,
        validator: [AuthorityGreater(AuthorityType.Owner)],
        worker: (player: PlayerCharacter, sender: Character, result: RegExpExecArray) => {
            let targetMessage = `${GetSelfRef(player)} obeys the orders of ${GetOwnerRef(player)}.`;
            InitiateStdTalkTask(player, sender, targetMessage);

            return undefined;
        }
    },
]

export default CommandList;