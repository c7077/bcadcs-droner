const StringSource = {
    system_loaded: "Advanced Drone Control System™ v{0} booted.",

    award_full_gear: "1 hour in full gear and bounded, {0}.",
    award_full_gear_bounded: "1 hour in full gear, {0}.",

    punish_orgasm: "Orgasmed unauthorizedly, {0}.",

    score_ctrl_reduce_1_penalty: "Penalty points decreased by 1, skill impairment reduced to {0}%",
    score_ctrl_increase_1_penalty: "Penalty points increased by 1, skill impairment increased to {0}%",
    score_ctrl_score_increase: "Reward points increased by {0}",
    shock_ctrl_engaged: "Shock punishment engaged, {0} times left.",
    shock_ctrl_finished: "Shock punishment ends.",
    ref_game_info: "Forbidden word “{0}” detected, correct usage : “{1}”.",

    outfit_collar_name: "ADCS™ Central Processor",
    outfit_collar_desc: "A completed Drone can only act according to the instructions of the control system. This portable system can provide Drone with localized command services anytime and anywhere.",
    outfit_harness_name: "ADCS™ Main Sensor",
    outfit_harness_desc: "Sensors are all over Drone's body. The external environment is monitored by various sensors. At the same time, it also monitors various physiological data of Drone.",
    outfit_chastity_name: "ADCS™ Sexual Management",
    outfit_chastity_desc: "Generally speaking, Drone can only act according to the instructions of the control system, but Drone's response to sexual stimulation is far beyond normal levels. With this property, it is possible to keep a Drone alive by controlling its sexual stimulation.",
    outfit_bra_name: "ADCS™ Com Center",
    outfit_bra_desc: "Covering the Drone's chest, it provides a variety of communication methods, provides an internal console and has a variety of interfaces. In order to be compatible with various systems, it takes up a large volume and is the main source of power consumption of the entire system.",
    outfit_visor_name: "ADCS™ Vision Control",
    outfit_visor_desc: "Through high-performance image processing, any image judged by the system as bad information can be mosaiced in real time.",
    outfit_ear_name: "ADCS™ Hearing Control",
    outfit_ear_desc: "The basic function of this module is to issue auditory instructions to Drone. In addition, ADCS™ aggressively intervenes in Drone's senses, and everything Drone hears is filtered by ADCS™.",
    outfit_arms_name: "ADCS™ Hand Movement Monitor",
    outfit_arms_desc: "This module monitors the Drone's arm movement data. It also provides the ability to restrict movement when needed.",
    outfit_hands_name: "ADCS™ Finger Control",
    outfit_hands_desc: "Fingers are used for delicate work, and the Drone may only uses them when its job requires it.",
    outfit_thighs_name: "ADCS™ Leg Control",
    outfit_thighs_desc: "Primary motion limiter. Cooperating with the ADCS™ Leg Monitor can make Drone lose the function of legs.",
    outfit_shanks_name: "ADCS™ Leg Monitor",
    outfit_shanks_desc: "This module monitors the Drone's leg movement data. It also provides the ability to restrict movement when needed.",
    outfit_feet_name: "ADCS™ Move Range Limiter",
    outfit_feet_desc: "The Drone is only authorized to move within a limited range, this module will lock the Drone to the ground if the Drone moves out of range.",

    outfit_group_name_legs: "Thighs",
    outfit_group_name_feet: "Shanks",

    task_do_reward_yes: `reward: yes`,
    task_do_reward_no: `reward: no`,
    task_do_penalty_yes: `penalty: yes`,
    task_do_penalty_no: `penalty: no`,
    task_finish_success: `Task success. ({0})`,
    task_finish_fail: `Task failed. ({0})`,
    task_punc_info_sep: ', ',
    task_talk_initiate: `Talk task initiated. ({0})`,
    task_talk_content: `content: “{0}”`,
    task_pose_initiate: `Pose task initiated. ({0})`,
    task_act_initiate: `Interact task initiated. ({0})`,
}

export const ref_game_regex = new RegExp('\\b(me|i|my|mine)\\b', 'iu');

export default StringSource;