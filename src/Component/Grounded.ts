import { ModSDKModAPI } from "bondage-club-mod-sdk";
import { IsInCollar } from "../Outift/OutfitCtrl";
import { DataManager } from "../Data";

export function GroundedCtrl(mod: ModSDKModAPI) {
    mod.hookFunction('ChatRoomCanLeave', 0, (args, next) => {
        if (Player && IsInCollar(Player)) {
            if (DataManager.instance.effects.test('Grounded')) return false;
        }
        return next(args);
    });
}