import bcMod from 'bondage-club-mod-sdk'
import { ExecCommands } from './ChatRoom/Commands';
import { DataManager } from './Data';
import { ADCS_CUSTOM_ACTION_TAG, ModName, ModVersion } from './Definitions';
import { SelfRefGameChat } from './Component/RefGame';
import { ADCSCheckTask, CheckChat } from './Task/Task';
import { ADCSDrawCharacter } from './Draw';
import { AwardFullGear, IncreasePunish } from './Component/ScoreControl';
import { ChatMessageHandler } from './utils/Chat/ChatMessageHandle';
import { ChatLogHandler } from './utils/Chat/ChatLogHandle';
import { ActivityHandle } from './ChatRoom/Activity';
import { hookOrgasmEvent } from './Component/OrgasmHandle';
import { ChatRoomAction, Monitor } from 'bc-utilities';
import { SkillImpair } from './Component/SkillImpair';
import { ShockUnit } from './Component/ShockProvider';
import { NoRelease } from './Component/NoRelease';
import { SendChatModifier } from './utils/Chat/SendChatModifier';
import { DroneTalk } from './Component/DroneTalk';
import { GroundedCtrl } from './Component/Grounded';
import { IsInCollar } from './Outift/OutfitCtrl';
import { ChatRoomStdLocalAction, GetString } from './Locale';
import { FuturisticBypass } from './Outift/FuturisticBypass';
import { DataEffect } from './Data';

(function () {
    if (window.__load_flag__) return;

    window.__load_flag__ = false;
    let mod = bcMod.registerMod({ name: ModName, fullName: ModName, version: ModVersion, repository: 'https://gitlab.com/dynilath/BCADCS' });

    Monitor.init(200).then(monitor => {
        AwardFullGear(monitor);
        ShockUnit.instance.init(monitor);
    })

    SkillImpair.instance.init(mod);


    GroundedCtrl(mod);

    mod.hookFunction('AsylumGGTSGetLevel', 10, (args, next) => {
        if (CurrentScreen === 'Login' || CurrentScreen === 'MainHall') {
            const data = DataManager.instance.data.ADCS;
            if (IsInCollar(Player as Character) && DataEffect().test('InjectGGTS')
                && data.Punish === 0 && data.Score > 0) {
                return 0;
            }
        }
        return next(args);
    });

    let initial_notify_not_shown = true;

    mod.hookFunction('ChatRoomRun', 20, (args, next) => {
        next(args);
        if (initial_notify_not_shown) {
            ChatRoomStdLocalAction(GetString("system_loaded", [`${ModVersion}`]));
            initial_notify_not_shown = false;
        }
    });

    const chatMessageHandler = new ChatMessageHandler;
    const chatLogHandler = new ChatLogHandler;

    mod.hookFunction('ChatRoomMessage', 10, (args, next) => {
        next(args);
        chatMessageHandler.Run(Player, args[0] as ServerChatRoomMessage);
    });

    mod.hookFunction('ChatRoomRun', 10, (args, next) => {
        if (Player) {
            ADCSCheckTask(Player);
            chatLogHandler.Run(Player);
        }
        next(args);
    });

    FuturisticBypass.init(mod);

    const chatMod = new SendChatModifier;

    chatMod.register(10, SelfRefGameChat);
    chatMod.register(0, DroneTalk);

    chatMod.init(mod);

    mod.hookFunction('OnlineGameDrawCharacter', 10, (args, next) => {
        if (Player) ADCSDrawCharacter(...(args as [Character, number, number, number]));
        next(args);
    })

    NoRelease(mod);

    chatMessageHandler.Register('Activity', ActivityHandle);

    chatLogHandler.Register((player, sender, msg) => {
        if (player.MemberNumber === sender.MemberNumber) {
            CheckChat(player, msg.Original);
        }
    });

    ChatRoomAction.init(ADCS_CUSTOM_ACTION_TAG);

    chatMessageHandler.Register('Chat', (player, sender, data) => {
        const strip = (s: string) => s.replace(/^[\p{P}\s~]/u, '');

        const unstuttered = data.Content.replace(/([a-zA-Z])(-\1)+/g, "$1");
        let strip1 = strip(unstuttered);

        const testS1 = `#${CharacterNickname(player)}`;
        const testS2 = `Drone ${player.MemberNumber}`;
        const testS3 = `${player.MemberNumber}`;
        if (strip1.startsWith(testS1)) {
            strip1 = strip1.slice(testS1.length);
        } else if (strip1.startsWith(testS2)) {
            strip1 = strip1.slice(testS2.length);
        } else if (strip1.startsWith(testS3)) {
            strip1 = strip1.slice(testS3.length);
        } else return;

        ExecCommands(strip(strip1), player, sender);
    });

    DataManager.init(mod, `${ModName} v${ModVersion} loaded.`);

    hookOrgasmEvent(mod, {
        Orgasm: (C) => { if (DataEffect().test('PunishOrgasm')) ChatRoomStdLocalAction(GetString('punish_orgasm', [IncreasePunish()])); },
        Resisted: (C) => { },
        Ruined: (C) => { },
    });

    console.log(`${ModName} v${ModVersion} loaded.`);
    window.__load_flag__ = true;
})();
