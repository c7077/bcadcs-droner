import { DroneName } from "../../../Contents";
import { DataManager, GetData } from "../../../Data";
import { ChatRoomAction } from "bc-utilities";
import { IsOwnedBy, IsSaotome, NotBlacklisted, Or } from "../../../utils/Criteria";
import { CommandUnit } from "../../CommandBasics";

const CommandList: CommandUnit[] = [
    {
        handler: /^(?:导出数据)/u,
        validator: [NotBlacklisted, Or(IsOwnedBy, IsSaotome)],
        worker: (player: Character, sender: Character) => {
            let msg = LZString.compressToBase64(JSON.stringify(GetData()));
            ChatRoomAction.instance.SendAction(`[ADCS] ${DroneName(player)} 处理数据中。`)
            ServerSend("ChatRoomChat", { Content: msg, Type: "Whisper", Target: sender.MemberNumber });
            return undefined;
        }
    },
    {
        handler: /^(?:导入数据)[\\p{P} ]+((?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?)/u,
        validator: [NotBlacklisted, Or(IsOwnedBy, IsSaotome)],
        worker: (player: Character, sender: Character, match: RegExpExecArray) => {
            DataManager.instance.loadRaw(match[1]);
            ChatRoomAction.instance.SendAction(`[ADCS] ${DroneName(player)} 处理数据成功。`);
            return undefined;
        }
    },
]

export default CommandList;